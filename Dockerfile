FROM ubuntu:focal

ARG APP_UID=2000

ARG APP_GID=2000

LABEL maintainer="TANGO Controls team <contact@tango-controls.org>"

ENV DEBIAN_FRONTEND=noninteractive

ENV CMAKE_PREFIX_PATH=/home/tango/lib/cmake
ENV PKG_CONFIG_PATH=/home/tango/lib/pkgconfig

RUN apt-get update -qq &&                     \
  apt-get install -y --no-install-recommends  \
  apt-utils                                   \
  build-essential                             \
  bzip2                                       \
  ca-certificates                             \
  clang-12                                    \
  clang-tools-12                              \
  clang-tidy-12                               \
  curl                                        \
  docker.io                                   \
  gdb                                         \
  git                                         \
  gpg                                         \
  iproute2                                    \
  libc-ares-dev                               \
  libcurl4-openssl-dev                        \
  libjpeg-dev                                 \
  libre2-dev                                  \
  libssl-dev                                  \
  libzmq3-dev                                 \
  libzstd-dev                                 \
  llvm-12                                     \
  pkg-config                                  \
  python3                                     \
  python3-dev                                 \
  ruby-dev                                    \
  sudo                                        \
  zlib1g-dev                               && \
  curl -L https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null                                       \
    | gpg --dearmor -                                                                                               \
    | tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null                                             && \
  echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ focal main' \
    | tee /etc/apt/sources.list.d/kitware.list >/dev/null                                                        && \
  apt-get update &&                                                                                                 \
  apt-get install -y --no-install-recommends                                                                        \
    cmake=3.18.0-0kitware1ubuntu20.04.1 cmake-data=3.18.0-0kitware1ubuntu20.04.1                                 && \
  rm -rf /var/lib/apt/lists/*

RUN groupadd -g "$APP_GID" tango                               && \
    useradd -l -u "$APP_UID" -g "$APP_GID" -ms /bin/bash tango && \
    usermod -a -G docker,sudo tango                            && \
    echo "%sudo ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers        && \
    ln -s /usr/lib/llvm-12/bin/llvm-symbolizer /usr/local/bin  && \
    echo "/home/tango/lib" > /etc/ld.so.conf.d/home.conf

USER tango

WORKDIR /home/tango

COPY --chown="$APP_UID":"$APP_GID" scripts/*.sh ./
COPY --chown="$APP_UID":"$APP_GID" scripts/patches/* ./patches/

RUN ./common_setup.sh               && \
    ./install_cppzmq.sh             && \
    ./install_omniorb.sh            && \
    ./install_tango_idl.sh          && \
    ./install_catch.sh              && \
    ./install_opentelemetry_deps.sh && \
    ./install_opentelemetry.sh      && \
    rm -rf dependencies
